aggressive-indent-mode (1.10.0+git20230112.1.a437a45-1) unstable; urgency=medium

  [ Sean Whitton ]
  * QA upload.
  * New upstream snapshot.
  * Update d/copyright for upstream copyright years & GPL version.
  * Orphan package.
    I haven't used aggressive-indent-mode for some years so I shouldn't be
    the one maintaining it.
    (I didn't do this before now because this was my first package.)

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 05 Sep 2024 11:43:27 +0100

aggressive-indent-mode (1.9.0-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:35:25 +0900

aggressive-indent-mode (1.9.0-3) unstable; urgency=medium

  * Team upload
  [ David Krauser ]
  * Update maintainer email address

  [ Dhavan Vaidya ]
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org

  [ David Bremner ]
  * Rebuild with dh-elpa 2.x
  * Remove debian/lintian-overrides as it was causing a lintian error.

 -- David Bremner <bremner@debian.org>  Fri, 25 Dec 2020 17:00:54 -0400

aggressive-indent-mode (1.9.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sat, 24 Aug 2019 15:47:21 -0300

aggressive-indent-mode (1.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix Version: header which upstream neglected to update.
  * Switch to dgit-maint-merge(7) workflow.
    - Add d/source/options
    - Add d/source/patch-header.
  * Bump std-ver to 4.1.1 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 22 Oct 2017 11:47:18 -0700

aggressive-indent-mode (1.8.3-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper compat & build-dep to 10.
  * Drop d/source/local-options for dgit.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 19 Oct 2016 21:07:47 -0700

aggressive-indent-mode (1.8.1-1) unstable; urgency=medium

  * Package new upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 02 Jun 2016 15:20:53 +0900

aggressive-indent-mode (1.7-1) unstable; urgency=medium

  * Package new upstream version.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 05 May 2016 12:47:53 -0700

aggressive-indent-mode (1.6-1) unstable; urgency=high

  * Package new upstream release.
    Fixes using aggressive-indent-mode with electric-pairs-mode.
  * Bump standards version to 3.9.8; no changes required.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 17 Apr 2016 08:00:18 -0700

aggressive-indent-mode (1.5.2-1) unstable; urgency=medium

  * Package new upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 05 Apr 2016 11:27:22 -0700

aggressive-indent-mode (1.5.1-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 02 Apr 2016 11:58:03 -0700

aggressive-indent-mode (1.5-1) unstable; urgency=medium

  * New upstream release.
  * Update patch to README.md.
  * Improve package description.
  * Override Lintian pedantic warnings with justifications.
  * Bump standards version to 3.9.7 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 02 Mar 2016 07:55:20 -0700

aggressive-indent-mode (1.4.2-2) unstable; urgency=medium

  * Fix minor error in debian/copyright file.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 03 Jan 2016 16:36:51 +0000

aggressive-indent-mode (1.4.2-1) unstable; urgency=low

  * Initial upload (Closes: #807426).

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 10 Dec 2015 12:30:22 -0700
